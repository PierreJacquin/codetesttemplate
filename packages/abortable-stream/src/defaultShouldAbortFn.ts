
let shouldAbord = false
export default function defaultShouldAbortFn(timeoutms = 4000) {
  
  setTimeout(() => shouldAbord = true, timeoutms)

  return () => {
    
    return shouldAbord
  }

}