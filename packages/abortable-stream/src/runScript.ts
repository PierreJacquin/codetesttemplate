

import abortableGenerator from './abortableGenerator.js'
import defaultShouldAbortFn from './defaultShouldAbortFn.js'

// npx ts-node src/runScript.tsx


console.log('===== Start processing')

const ms_timeout = 40 // do not hesitate to increase it for more chunks & documents processing
const abortFn = defaultShouldAbortFn(ms_timeout)

for await(const document of abortableGenerator(abortFn)){

  console.log('Processing document', document.id)
  await new Promise(resolve => setTimeout(resolve, 1));

}

console.log('===== Generator loop was gracefully aborted')
