# Mindmatcher - Python Technical Test

* Welcome to your coding test repository. 
* See below for instructions. 

# Requirements

pip install -r requirements.txt

## Exercice Objective and instruction

* The first objective is to deploy both models on a simple API.
* Next you will stream the file in the data folder and performs these operations:
    - You will look only at the items with "esco:Occupation" type.
    - Embed the text in the "description" -> "literal" field using the API
    - Use the API classification model to discard non-relevant elements (1 for relevant, 0 for not relevant).
    - Export the remaining elements to a properly formated json.

### Requirement 1: Implementation

The API should have two endpoints : 
* one for embedding, input is a string, output is a 1024 dimension tensor
* one for classification, input is a 1024 dimension tensor, output is 0 or 1

### Requirement 2: Unit Testing / Logs / Packaging

We will pay particular attention to code etiquette and documentation.
* Provide Unit Testing for both API endpoints as well as typical edge cases and errors.
* Implement logging throughout the application with the python logging library
* Package your application for easy deployment, with a "setup.py" file specifying dependencies, entry points, and other relevant metadata.

## Good to know

### Embedding

Embedding model can be charged using:
    model = sentence_transformers.SentenceTransformer("models/embedding")
and can be used to get embedding with:
    embedding = model.encode(text,convert_to_tensor=True)

### Classification

Classification model can be charged using:
    model = pickle.load(open("models/classification.sav", 'rb'))  # Chargement du modèle pré-entraîné
and can be used to get prediction with:
    prediction = model. predict(embedding)

## Note

The goal of this test is not for you to spent all day making the perfect app, do what's reasonable within the imparted time.
We will discuss your choices as well as what you would have done differently without the time constraint.
